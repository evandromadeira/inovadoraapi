package br.com.thecatapi.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "weight")
public class Weight implements Serializable{

	private static final long serialVersionUID = -7332982672785980456L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String imperial;
	private String metric;

	public void setId(Long id) {
		this.id = id;
	}
	public String getImperial() {
		return imperial;
	}
	public void setImperial(String imperial) {
		this.imperial = imperial;
	}
	public String getMetric() {
		return metric;
	}
	public void setMetric(String metric) {
		this.metric = metric;
	}	
}
