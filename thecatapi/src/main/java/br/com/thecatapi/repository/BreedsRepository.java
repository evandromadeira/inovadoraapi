package br.com.thecatapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.thecatapi.models.Breeds;

public interface BreedsRepository extends JpaRepository<Breeds, String> {

}