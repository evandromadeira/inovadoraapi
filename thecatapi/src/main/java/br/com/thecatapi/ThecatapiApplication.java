package br.com.thecatapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class ThecatapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThecatapiApplication.class, args);
	}
}