package br.com.thecatapi.resources;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.thecatapi.models.AuthenticationRequest;
import br.com.thecatapi.models.AuthenticationResponse;
import br.com.thecatapi.models.Breeds;
import br.com.thecatapi.repository.BreedsRepository;
import br.com.thecatapi.services.MyUserDetailsService;
import br.com.thecatapi.util.JwtUtil;

@RestController
@RequestMapping(value = "/api")
public class BreedsResource {

	@Autowired
	private BreedsRepository breedsRepository;

	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private MyUserDetailsService userDetailsService;
	
	@Autowired
	private JwtUtil jwtTokenUtil;

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest)
			throws Exception {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
					authenticationRequest.getUesrname(), authenticationRequest.getPassword()));
		} catch (BadCredentialsException e) {
			throw new Exception("Usuário ou senha incorretos!", e);
		}
		final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUesrname());
		
		final String jwt = jwtTokenUtil.generateToken(userDetails);

		return ResponseEntity.ok(new AuthenticationResponse(jwt));
	}

	@GetMapping("/breeds")
	@Cacheable(value = "breeds")
	public List<Breeds> findBreeds() throws InterruptedException {
		Thread.sleep(3000); //Testing cache
		return breedsRepository.findAll();
	}

	@GetMapping("/breeds/id/{id}")
	@Cacheable(value = "breeds")
	public Optional<Breeds> findBreedById(@PathVariable(name = "id", required = true) String id) throws InterruptedException {
		Thread.sleep(3000); //Testing cache
		return breedsRepository.findById(id);
	}

	@PostMapping("/breeds/save")
	@CacheEvict(value = "breeds", allEntries = true)
	public Breeds saveBreed(@RequestBody Breeds breed) {
		return breedsRepository.save(breed);
	}

	@DeleteMapping("/breeds/delete")
	public void deleteBreed(@RequestBody Breeds breed) {
		breedsRepository.delete(breed);
	}

	@PutMapping("/breeds/update")
	@CacheEvict(value = "breeds", allEntries = true)
	public Breeds updateBreed(@RequestBody Breeds breed) {
		return breedsRepository.save(breed);
	}
}